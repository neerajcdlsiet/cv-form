import React, { useEffect, useState } from "react";
import './admin.css'
import 'antd/dist/antd.css';
import { FaUserAlt } from "react-icons/fa";
import { Button, Dropdown, Menu, Space } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import { setAuthUser } from "../store/redux";
import { jsPDF } from "jspdf";
import Parangat_banner from '../img/Parangat_banner.png'
import { convertToTitle } from "../helpers";


const Admin = () => {
    const dispatch = useDispatch();
    const { auth } = useSelector(state => state.authReducer);
    const handleSignOut = () => {
        dispatch(setAuthUser({}))
    }
    const [data, setData] = useState([]);
    // const doc = new jsPDF();

    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: (
                        <button className="bg-transparent border-0" target="_blank" href="#" onClick={handleSignOut}>
                            Log Out
                        </button>
                    ),
                },
            ]}
        />
    );

    const loadData = async () => {
        const response = await fetch('https://cv.abcxyz.in/api/getAllResumes', {
            method: "GET",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json",
                "Authorization": auth.token
            },
        });
        if (!response) return
        if (response.status === 200) {
            let result = await response.json();
            if (result.statusCode === 200) {
                setData(result.data)
            }
        }
    }

    // view cv data

    const afterSubmitView = () =>{
        console.log("this is view data");
    }

    //pdfGeneratedHandler 
    const pdfGeneratedHandler = (item) => {
        var doc = new jsPDF('portrait', 'px', 'a4', 'false')
        doc.addImage(Parangat_banner, 'PNG', 0, 0, 445, 650,);
        doc.text("Resume", 180, 80)
        var x = 40;
        var y = 150
        Object.keys(item).map(key => {
            if (item[key]) {
                if (typeof item[key] === "string" || typeof item[key] === 'number') {
                    const title = convertToTitle(key.replace("_", " "))
                    doc.text(title + ":", x, y)

                    doc.text(item[key], x + 100, y, { maxWidth: 300, lineHeightFactor: 3 })
                    y = y + 20
                } if (Array.isArray(item[key])) {

                }
            }
        })

        doc.save('Employee_Resume.pdf')
    }


    useEffect(() => {
        loadData()
    }, []);

    return (
        <div className="table-container">
            <div className="table-wrapper">
                <div className="table-heading">
                    <h1>Log Out</h1>
                    <Space wrap>
                        <Dropdown overlay={menu} >
                            <Button><FaUserAlt /></Button>
                        </Dropdown>
                    </Space>
                </div>
                <div className="table-data">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">NAME</th>
                                <th scope="col">EMP ID</th>
                                <th scope="col">EMAIL ID</th>
                                <th scope="col">CONTACT</th>
                                <th scope="col">VIEW</th>
                                <th scope="col">DOWNLOAD</th>
                            </tr>
                        </thead>
                        <tbody>

                            {data.map((fields, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{fields.first_name}</td>
                                        <td>{fields.employee_id}</td>
                                        <td>{fields.email}</td>
                                        <td>{fields.phone_number}</td>
                                        <td><button className="view-btn" onClick={afterSubmitView}>View</button></td>
                                        <td><button className="download-btn" onClick={() =>
                                            pdfGeneratedHandler({
                                                first_name: fields.first_name,
                                                employee_id: fields.employee_id,
                                                designation: fields.designation,
                                                summary: fields.summary,
                                                skills: fields.skills,
                                                projects: fields.projects
                                            })}>Download</button></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Admin;
