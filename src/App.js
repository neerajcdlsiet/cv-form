import './App.css';
import MakeResume from './dashboard/MakeResume';
import Login from './login/Login';
import Admin from './admin/Admin';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useSelector } from 'react-redux'
import SubmittedMessage from './SubmittedMessage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



function App() {
  // const currentUser = useSelector(state=>state.authReducer.auth);
  const currentUser = useSelector(({ authReducer: { auth } }) => auth);

  return (
    <div className="App">
      <ToastContainer />
      <Router >
        <Routes>
          {Object.keys(currentUser).length === 0 ?
            <Route exect path='/' element={<Login />} /> :
            <>
              <Route exect path='/' element={<Admin />} />

            </>
          }
          <Route exect path='/resume' element={<MakeResume />} />
          <Route exect path='/submitted' element={<SubmittedMessage />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
