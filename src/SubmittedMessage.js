import React from "react";
import './SubmittedMessage.css'
import {useNavigate} from 'react-router-dom'

const SubmittedMessage = () => {
    const navigate = useNavigate();

    const anotherCvSubmitHandler = () =>{
        navigate('/')
    }
  

    return (
        <div className="submitted-cv-container">
            <div className="submitted-cv-wrapper">
                <div className="submitted-cv">
                    <h1>Your submission has been <br /> successful! </h1>
                </div>
                <p>Thanks! We have received your submission</p>
                <button className="btn-submit-another" onClick={anotherCvSubmitHandler}>Submit another</button> 
            </div>
        </div>
    )
}

export default SubmittedMessage;