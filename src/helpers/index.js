// for toast

import { toast } from "react-toastify";
export const showToastMessage = (message, type = true) => {
    if (type) {
        toast.success(message, { position: toast.POSITION.TOP_RIGHT });
    } else {
        toast.error(message, { position: toast.POSITION.TOP_RIGHT });
    }
}

export function convertToTitle(str) {
    if ((str === null) || (str === ''))
        return false;
    else
        str = str.toString();

    return str.replace(/\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
}

// validation of Email in login page

export function isValidEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
}