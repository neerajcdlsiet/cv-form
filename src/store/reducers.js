import { combineReducers } from "@reduxjs/toolkit";
import * as allReducers from "./redux";


export const rootReducer = combineReducers(allReducers);
