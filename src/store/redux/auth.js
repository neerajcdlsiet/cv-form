const SET_AUTH_USER = "SET_AUTH_USER";

export const setAuthUser = (data) => (dispatch) => {
  dispatch({
    type: SET_AUTH_USER,
    payload: data,
  });
};

const initialeState = {
  auth: {},
};

export const authReducer = (state = initialeState, action) => {
  switch (action.type) {
    case SET_AUTH_USER:
      return {
        ...state,
        auth: action.payload,
      };
    default:
      return state;
  }
};
