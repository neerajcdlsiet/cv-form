import React, { useState } from "react";
import './Login.css';
import { FaUserAlt, FaUnlock } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setAuthUser } from "../store/redux";
import { showToastMessage } from "../helpers";


const Login = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        email: "", password: ""
    });

    const formHandleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setFormData({
            ...formData,
            [name]: value
        })
    }

    const loginHandler = async (e) => {
        e.preventDefault();
        
        const response = await fetch('https://cv.abcxyz.in/api/login', {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(formData)
        });
        if (!response) return
        if (response.status === 200) {
            let result = await response.json();
            if (result.statusCode === 200) {
                dispatch(setAuthUser(result?.data))
                showToastMessage("Login Success")
            } else {
                showToastMessage("Something went wrong", false)
            }

        } else {
            showToastMessage("Something went wrong", false)
        }
    }


    return (
        <div className="login-container">
            <div className="login-wrapper">
                <img src={require("../img/logo_parangat.png")} />
                <h1>LOGIN</h1>
                <div className="login-form">
                    <form onSubmit={loginHandler}>
                        <div className="login-form-field">
                            <div className="username">
                                <FaUserAlt />
                                <input autoComplete="off" name="email" value={formData.email}
                                    type="text" placeholder="username"
                                    onChange={formHandleChange} required />
                            </div>
                            <div className="password">
                                <FaUnlock />
                                <input autoComplete="off" name="password" value={formData.password}
                                    type="password" placeholder="password"
                                    onChange={formHandleChange} required />
                            </div>

                        </div>
                        <div className="login-submit-btn">
                            <button className="btn1" type="submit">login</button>
                            <button className="btn2" onClick={() => { navigate('/resume') }}>Submit Your cv</button>
                            <span className="guest-user">Guest User goes to Submit Your CV</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login;