import React, { useEffect, useState } from "react";
import './MakeResume.css';
import { useNavigate } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { showToastMessage } from "../helpers";

const MakeResume = () => {
    const [hideForm, setHideForm] = useState(false);
    const navigate = useNavigate()
    const [model, setModel] = useState({
        first_name: "",
        last_name: "",
        email: "",
        phone_number: "",
        designation: "",
        summary: "",
        skills: "",
        projects: []
    });


    const [project, setProject] = useState({
        title: "",
        description: ""
    });
    const [summaryData, setSummaryData] = useState([])


    const handleInput = (e, type) => {
        let name = e?.target?.name
        let value = e?.target?.value

        // if (type === "string") {
        //     let str = model.summary;
        //     let splitOfStr = str.split(',');
        //     console.log("splitOfStr", splitOfStr)

        //     setSummaryData({
        //         ...splitOfStr
        //     })
        //     console.log("summaryData", summaryData)
        // }

        if (type === "array") {
            let array = [...model[name]]
            array.push(value)
            value = array
            setProject({
                title: '',
                description: ''
            })
        }
        setModel({
            ...model,
            [name]: value  // es value me string or array dono ki value add hai
        })
    }

    // project handle change
    const projectOnChangeHandler = (e) => {
        setProject({
            ...project,
            [e.target.name]: e.target.value
        })
    }

    const submitResumeHandler = async (e) => {
        e.preventDefault();
        const response = await fetch('https://cv.abcxyz.in/api/addResume', {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(model)
        });
        if (!response) return
        if (response.status === 200) {
            let result = await response.json();
            if (result.statusCode === 200) {
            }
        }

        if (model.first_name === "") {
            showToastMessage("first name is required!", false)
        } else if (model.last_name === "") {
            showToastMessage("last name is required!", false)
        } else if (model.email === "") {
            showToastMessage("email is required!", false)
        } else if (model.phone_number === "") {
            showToastMessage("phone number is required!", false)
        } else if (model.designation === "") {
            showToastMessage("designation is required!", false)
        } else if (model.summary === "") {
            showToastMessage("summary is required!", false)
        } else if (model.skills === "") {
            showToastMessage("skills are required!", false)
        } else if (model.projects === "") {
            showToastMessage("projects are required!", false)
        } else {
            navigate('/submitted');
            showToastMessage("form submitted successfully",)
        }
    }

    const beforeSubmitView = () => {
        console.log("this first view")
    }


    return (
        <div className="makeresume-container">
            <h2>Cv-Form</h2>
            <div className="makeresume-wrapper">
                <div className="resume-field-part" style={{ display: hideForm ? 'none' : 'block' }}>
                    <div className="form">
                        <form>
                            <div className="form-row firstname-lastname-part">
                                <div className="col">
                                    <label>First Name</label>
                                    <input type="text" required className="form-control" name="first_name"
                                        value={model.first_name} onChange={handleInput} placeholder="first name" />
                                </div>
                                <div className="col">
                                    <label>Last Name</label>
                                    <input type="text" className="form-control" name="last_name"
                                        value={model.last_name} onChange={handleInput} placeholder="last name" />
                                </div>
                            </div>
                            <div className="form-row email-phone-part">
                                <div className="col">
                                    <label>Email</label>
                                    <input type="email" required className="form-control" name="email"
                                        value={model.email} onChange={handleInput} placeholder="email" />
                                </div>
                                <div className="col">
                                    <label>Phone no</label>
                                    <input type="number" required className="form-control" name="phone_number"
                                        value={model.phone_number} onChange={handleInput} placeholder="phone" />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col designation">
                                    <label>Designation</label>
                                    <input type="text" required className="form-control" name="designation"
                                        value={model.designation} onChange={handleInput} placeholder="designation" />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col add-summary">
                                    <label>Summary</label>
                                    <div className="summary-parts">
                                        <input type="text" required name="summary" className="form-control summary"
                                            value={model.summary}
                                            onChange={handleInput} placeholder="follow the sample below" />
                                    </div>

                                    <div className="sample1">
                                        <ul>
                                            <li>4+ years of experience in Nodejs, Strong knowledge of object-oriented programming</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div className="form-row">
                                <div className="col add-skill">
                                    <label>Skill</label>
                                    <div className="skills-parts">
                                        <input type="text" required name="skills"
                                            className="form-control skills"
                                            value={model.skills}
                                            onChange={handleInput} placeholder="follow the sample below" />
                                    </div>
                                    <div className="sample1">
                                        <ul>
                                            <li>Programming Languages: Javascript, Python, C, C++,Lua</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col add-projects">
                                    <label>Major Projects worked on</label>
                                    <div className="entry-project-part">
                                        <div className="project-heading">
                                            <input required placeholder="Project 1: Member App:" name="title"
                                                value={project.title}
                                                onChange={(projectOnChangeHandler)} />
                                        </div>
                                        <div className="project-desc">
                                            <textarea name="description" required className="form-control" value={project.description}
                                                onChange={projectOnChangeHandler} placeholder="description of this project" />
                                        </div>
                                    </div>
                                    <button className="btn project-btn" type="button"
                                        onClick={() => handleInput({ target: { name: 'projects', value: project } }, "array")}
                                        style={{ float: "right" }}>Add Project</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {/* *************************** cv right part ********************************* */}

                <div className="resume-build-container">
                    <div className="resume-build-part" id="resume-build-container">
                        <div className="show-cv-wrapper">
                            <div className="first-last-name">
                                <div className="first-name">
                                    <h5><span>First Name :</span> {model.first_name}</h5>
                                </div>
                                <div className="last-name">
                                    <h5><span>Last Name :</span> {model.last_name}</h5>
                                </div>
                            </div>

                            <div className="email-phone-past">
                                <div className="email-address">
                                    <h5><span>Email :</span> {model.email}</h5>
                                </div>
                                <div className="phone-number">
                                    <h5><span>Phone :</span> {model.phone_number}</h5>
                                </div>
                            </div>
                            <div>
                                <h5><span>Designation :</span> {model.designation}</h5>
                            </div>

                            <div className="summary-part">
                                <h5><span>Summary :</span></h5>
                                {/* <p>{model.summary}</p> */}

                                {/* <ul>{summaryData.map((a) => {
                                    return <li>{a.summary}</li>
                                })}</ul> */}
                            </div>
                            <div className="show-skill-heading">
                                <h5><span>Skills :</span></h5>
                                <div className="show-skill-part">
                                    {/* <p>{model.skills}</p> */}
                                </div>
                            </div>
                            <div className="show-project">
                                <h5><span>Major Projects worked on :</span></h5>
                                {model.projects.map(((item, index) => {
                                    return <div key={index}>
                                        <div className="show-project-title">
                                            <h6><b>{item?.title}</b></h6>
                                        </div>
                                        <div className="show-project-desc">
                                            <p>{item?.description}</p>
                                        </div>
                                    </div>
                                }))}
                            </div>
                        </div>
                    </div>

                    <div className="projects-part">
                        {hideForm ? <button type="button" onClick={() => setHideForm(false)} className="viewCrossImg">
                            Back
                        </button> :
                            <button className="btn view-btn" onClick={beforeSubmitView} >View</button>
                        }
                        <button className="btn submit-btn" type="button" onClick={submitResumeHandler}>Submit</button>
                        <ToastContainer />
                    </div>
                </div>
            </div>
        </div>
    )
}


export default MakeResume;
